jQuery(document).ready(function () {
  jQuery('#login').click(function () {
    navigator.id.request();
  });
  jQuery('#logout').click(function () {
    navigator.id.logout();
  });
  navigator.id.watch({
    loggedInUser: turboCaseInfo.user,
    onlogin: function (assertion) {
      jQuery('#persona_assertion input[name=assertion]').val(assertion);
      jQuery('#persona_assertion').submit();
    },
    onlogout: function () {
      jQuery('#logout').click();
    }
  });
});
