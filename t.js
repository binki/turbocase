/* -*- Mode: js2; -*- */
'use strict';
var _ = require('lodash');

var entities = function (s) {
  if (!s && s !== '')
    throw new Error('s must be defined');
  return s.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
};
exports.entities = entities;

var RenderedHTML = function (html) {
  this.toHTML = function () {
    return html;
  };
  this.toString = this.toHTML;
};
exports.RenderedHTML = RenderedHTML;

var renderAsHTML = function (o) {
  if (!o)
    return '';
  if (o.toHTML)
    return o.toHTML();
  if (!_.isString(o) && o.length !== undefined)
    return _.map(o, renderAsHTML).join('');
  return entities(String(o));
};
exports.renderAsHTML = renderAsHTML;

function attributes (attributes) {
  return _.reduce(attributes, function (result, value, key) {
    if (value === undefined)
      return result;
    return result + ' ' + key + '="' + entities(String(value)) + '"';
  }, '');
}

function element (name, content, options) {
  options = options || {};
  var result = '<' + name + (attributes(options.attributes));
  if (content !== '' && !content)
    result += '/>';
  else
    result += '>' + renderAsHTML(content) + '</' + name + '>';
  return new RenderedHTML(result);
}

/*
 * Handle nonspecial elements.
 */
_.each([
  'button',
  'code',
  'dd',
  'div',
  'dl',
  'dt',
  'em',
  'fieldset',
  'form',
  'h1',
  'label',
  'legend',
  'li',
  'ol',
  'p',
  'td',
  'th',
  'tr',
  'ul',
], function (name) {
  exports[name] = function (content, options) {
    return element(name, content, options);
  };
});

exports.input = function (options) {
  return element('input', undefined, options);
};

exports.table = function (headers, rows, options) {
  return element(
    'table',
    [
      exports.thead(headers),
      exports.tbody(rows),
    ],
    options);
};

exports.tbody = function (rows, options) {
  return element(
    'tbody',
    _.map(rows, function (row) { return ['\n', exports.tr(row)]; }),
    options);
};

exports.thead = function (headers, options) {
  return element(
    'thead',
    exports.tr(
      _.map(headers, function (header) { return exports.th(header); })),
    options);
};
