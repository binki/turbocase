#!/usr/bin/env node
/* -*- Mode: js2; -*- */
'use strict';
var Cookies = require('cookies');
var Crypto = require('crypto');
var Fcgi = require('node-fastcgi');
var Moment = require('moment');
var Url = require('url');
var Sequelize = require('sequelize');
var SimpleGlob = require('./simpleglob');
var _ = require('lodash');
var t = require('./t');

var myConfig = require('./config.json');

var idpPersona = require('idp-persona')({audience: myConfig.personaAudience});
var libs = require('bower-files')().self();

var sequelize = new Sequelize(myConfig.db);


var modelInfos = {};
var mySequelizeDefine = function (modelName, modelColumns, details) {
  details = _.defaults({}, details, {actions: []});
  var modelInfo = {
    columns: _.mapValues(modelColumns, function (columnInfo, key) {
      var type = columnInfo.type || columnInfo;
      return {
        allowNull: columnInfo.allowNull !== false,
        autoIncrement: !!columnInfo.autoIncrement,
        friendlyName: columnInfo.friendlyName || key,
        name: key,
        render: function (value) {
          if (type === Sequelize.DATEONLY)
          {
            value = Moment(value);
            if (value.isValid())
              value = value.format('YYYY-MM-DD');
            else
              value = null;
          }
          if (type === Sequelize.DATE)
          {
            value = Moment(value);
            if (value.isValid())
              value = value.toISOString();
            else
              value = null;
          }
          if (type === Sequelize.INTEGER)
            value = String(value);
          return value || '';
        },
        type: type
      };
    })
    ,order: _(
      _.reduce(modelColumns, function (result, value, key) {
        result[key] = {
          key: key,
          weight: value.weight|0
        };
        return result;
      }, {}))
      .sortByAll('weight', 'key')
      .map('key')
      .value()
    ,actions: _.indexBy(details.actions, 'id')
  };
  delete details.actions;
  modelInfo.model = sequelize.define(modelName, modelColumns, details);
  modelInfos[modelName] = modelInfo;
  return modelInfo.model;
};

var userHasVisibility = function (user, visibility) {
  return !!_.find(
    user.Grants.concat([{grant: 'public'}]),
    userGrant => userGrant.grant === visibility);
};

var userCanAccess = function (user, path) {
  if (!user.Grants)
    throw new Error('You must have loaded the user with Grants to query access.');

  return userHasVisibility(user, routes[path].visibility);
};

/**
 * \brief
 *   Construct a new action.
 *
 * \param id
 *   slug
 * \param name
 *   Human-readable name.
 * \param visibility
 *   The grant required for action to be shown to/used by user.
 * \param execute
 *   A function (context){}. The model instance is “this”. Return
 *   array or a promise which resolves to an array to indicate
 *   redirect destination.
 * \param options
 *   Plain object with keys.
 *   - predicate: Called with model instance in “this”, return falsey
 *       to indicate that the action does not presently apply to the
 *       instance.
 */
var ModelAction = function (id, name, visibility, execute, options) {
  options = _.defaults({}, options, {
    predicate: function () {
      return true;
    }
  });

  this.id = id;
  this.name = name;
  this.userAllowed = function (user, instance) {
    return userHasVisibility(user, visibility) &&
      options.predicate.call(instance);
  };
  this.execute = function (context, instance) {
    if (!this.userAllowed(context.user, instance))
      throw new Error('Attempt to perform unpermitted action.');
    Promise.resolve(execute.apply(instance, [context].concat(_.slice(arguments, 2)))).then(function (redirectUrlArguments) {
      redirect.apply(
        this,
        [context].concat(redirectUrlArguments || ['index']));
    });
  };
  Object.freeze(this);
};

var User = mySequelizeDefine(
  'User',
  {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    email: {
      type: Sequelize.STRING,
      validate: { isEmail: true }
    }
  },
  {
    instanceMethods: {
      canAccess: function (path) {
        return userCanAccess(this, path);
      }
    }
  });
var Grant = mySequelizeDefine(
  'Grant',
  {
    grant: {
      primaryKey: true,
      type: Sequelize.STRING
    }
  });
User.belongsToMany(Grant, { through: 'UserGrants' });
Grant.belongsToMany(User, { through: 'UserGrants' });
var UserSession = mySequelizeDefine(
  'UserSession',
  {
    session: {
      primaryKey: true,
      type: Sequelize.STRING
    }
  });
User.hasMany(UserSession, {
  foreignKey: {
    allowNull: false
  }
});

var Client = mySequelizeDefine(
  'Client',
  {
    id: {
      friendlyName: 'Client Id',
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
      weight: -1
    },
    firstName: {
      friendlyName: 'First Name',
      type: Sequelize.STRING
    },
    middleName: {
      friendlyName: 'Middle Name',
      type: Sequelize.STRING
    },
    surname: {
      friendlyName: 'Last Name',
      type: Sequelize.STRING
    },
    birthDate: {
      friendlyName: 'Birthday',
      type: Sequelize.DATEONLY,
      weight: 1
    }
  },
  {
    instanceMethods: {
      formatName() {
        return _.filter([this.firstName, this.middleName, this.surname]).join(' ');
      }
    }
  });

var ServiceSource = mySequelizeDefine(
  'ServiceSource',
  {
    id: {
      autoIncrement: true
      ,friendlyName: 'Service Source Id'
      ,primaryKey: true
      ,type: Sequelize.INTEGER
      ,weight: -1
    }
    ,name: {
      friendlyName: 'Name',
      type: Sequelize.STRING
    }
    ,active: {
      friendlyName: 'Active'
      ,type: Sequelize.BOOLEAN
    }
  },
  {
    actions: [
      new ModelAction('activate', 'Activate', 'admin', function (context) {
        return this.update({active: true}).then(function () {
          return ['service-source'];
        });
      }, {
        predicate: function () {
          return !this.active;
        }
      })
      ,new ModelAction('deactivate', 'Deactivate', 'admin', function (context) {
        return this.update({active: false}).then(function () {
          return ['service-source'];
        });
      }, {
        predicate: function () {
          return this.active;
        }
      })
    ]
  });

var Service = mySequelizeDefine(
  'Service',
  {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
      weight: -1
    }
    ,name: {
      friendlyName: 'Name'
      ,type: Sequelize.STRING
    }
    ,active: {
      friendlyName: 'Active'
      ,type: Sequelize.BOOLEAN
    }
  },
  {
    actions: [
      new ModelAction('activate', 'Activate', 'admin', function () {
        return this.update({active: true}).then(function () {
          return ['service'];
        });
      }, {
        predicate: function () {
          return !this.active;
        }
      })
      ,new ModelAction('deactivate', 'Deactivate', 'admin', function () {
        return this.update({active: false}).then(function () {
          return ['service'];
        });
      }, {
        predicate: function () {
          return this.active;
        }
      })
    ]
  });
ServiceSource.hasMany(Service, {
  foreignKey: {
    allowNull: false
  }
});
Service.belongsTo(ServiceSource, {
  foreignKey: {
    allowNull: false
  }
});

var ServiceProvision = mySequelizeDefine(
  'ServiceProvision',
  {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
      weight: -1
    },
    timeProvided: {
      allowNull: false,
      friendlyName: 'Time',
      type: Sequelize.DATE
    }
  }
  ,{
    actions: [
      new ModelAction('delete', 'Delete', 'admin', function (context) {
        return this.destroy().then(function () {
          return ['provision'];
        });
      })
    ]
  }
);
ServiceProvision.belongsTo(Service, {
  foreignKey: {
    allowNull: false
  }
});
ServiceProvision.belongsTo(Client, {
  foreignKey: {
    allowNull: false
  }
});
ServiceProvision.belongsTo(User, {
  foreignKey: {
    allowNull: false
  }
});

var ensureUser = function (email) {
  /* Generate user if necessary */
  return User.findOrCreate({where: {email: email}}).then(function (userFindCreateInfo) {
    /*
     * When just created, an object is not as real as it is after it
     * is saved?
     */
    return userFindCreateInfo[0].save().then(function () {
      /* Now we know the user id. Now load with everything. */
      return User.find({
        where: {id: userFindCreateInfo[0].id},
        include: [Grant, ]
      });
    });
  });
};

var startupPromises = [];
startupPromises.push(sequelize.sync().then(function () {
  /*
   * Give administrators “admin” visibility.
   */
  return Promise.all(
    _.map(myConfig.administrators, administrator => ensureUser(administrator).then(function (user) {
      if (!_.find(user.Grants, userGrant => userGrant.grant === 'admin'))
        return Grant.findOrCreate({where: {grant: 'admin'}}).then(function (grants) {
          return grants[0].save();
        }).then(function (grant) {
          return user.addGrant(grant);
        });
      return user;
    })));
}));

var entities = t.entities;

var url = function (path, options) {
  options = options || {};
  options.query = options.query || {};
  options.query.path = path;
  return (
    'index.cgi?'
      + _.map(
       options.query,
       function (v, k) { return encodeURIComponent(k) + '=' + encodeURIComponent(v); }).join('&'));
};

var modelInfoByModel = function (model) {
  var modelInfo = _.find(modelInfos, mi => mi.model === model);
  if (!modelInfo)
    throw new Error('Unable to find modelInfo for given model.');
  return modelInfo;
};

var form = function (model, options) {
  var modelInfo = modelInfoByModel(model);
  options = _.defaults(
    {},
    options,
    {
      action: undefined,
      autofocus: false,
      hideAutoIncrement: true,
      method: 'post',
      /* Ugly hax for now */
      pre: ''
    });
  var autofocus = options.autofocus;
  /*
   * Because query parameters in action are dropped by the browser,
   * manually remove them and pass via <input type="hidden"/>.
   */
  var hidden = {};
  if (options.action)
  {
    var actionUrl = Url.parse(options.action, true);
    _.forEach(actionUrl.query, function (value, key) {
      hidden[key] = value;
      delete actionUrl.query[key];
    });
    delete actionUrl.search;
    options.action = Url.format(actionUrl);
  }
  return t.form([
    /* Ugly hax for now */
    options.pre,
    _(modelInfo.order)
      .map(function (key) {
        var column, type, value;

        column = modelInfo.columns[key];
        /* Do not generate form entries for autoincrement */
        if (options.hideAutoIncrement && column.autoIncrement)
          return '';

        type = 'text';
        if (column.type === Sequelize.DATEONLY)
        {
          type = 'date';
          value = Moment().format('YYYY-MM-DD');
        }
        if (column.type === Sequelize.TIME)
        {
          type = 'time';
          value = Moment().format('HH:mm:ssZ');
        }
        if (column.type === Sequelize.DATE)
        {
          /*
           * Include both date and time components. Hrm. For now
           * pretend that datetime is still a thing even though it was
           * dropped from the standard and will degrade to text
           * everywhere. Allow client-side JavaScript code recognize
           * it as an extension and build fanciness around it in the
           * future even…
           */
          type = 'datetime';
          value = Moment().format();
        }

        var autofocusValue = autofocus ? 'autofocus' : undefined;
        autofocus = false;

        return t.div(t.label([
          column.friendlyName,
          ': ',
          t.input({
            attributes: {
              autofocus: autofocusValue,
              name: column.name,
              type: type,
              value: value
            }
          })]));
      }).value(),
    _.map(hidden, function (value, key) {
      return t.input({
        attributes: {
          name: key,
          type: 'hidden',
          value: value
        }
      });
    }),
    t.div(t.button('Submit')),
  ], {
    attributes: {
      action: options.action,
      method: options.method
    }
  });
};

/**
 * \param options
 *   Keys:
 *   - create: function (data) that returns a promise for a model instance to create through alternate means, such as through a relationship .createAssociation().
 */
var submission = function (context, model, options) {
  options = _.defaults({}, options, {
    create: function (data) {
      return model.create(data);
    }
  });
  var modelInfo = modelInfoByModel(model);
  readFormPost(context).then(function (postData) {
    /* Perform validations we know about. */
    var validationFailures = [];
    _.forEach(modelInfo.columns, function (column) {
      var value = postData[column.name];
      var moment;

      if (column.type === Sequelize.DATEONLY)
      {
        if (!value)
        {
          postData[column.name] = null;
          if (!column.allowNull)
            validationFailures.push(column);
        }
        else
        {
          moment = Moment(value, 'YYYY-MM-DD', true);
          if (!moment.isValid())
            validationFailures.push(column);
          else
            value = moment.format('YYYY-MM-DD');
        }
      }
      if (column.type === Sequelize.DATE)
      {
        if (!value)
        {
          postData[column.name] = null;
          if (!column.allowNull)
            validationFailures.push(column);
        }
        else
        {
          moment = Moment(value, Moment.ISO_8601, true);
          if (!moment.isValid())
            validationFailures.push(column);
          else
            value = moment.toISOString();
        }
      }

      postData[column.name] = value;
    });
    if (validationFailures.length) {
      page(context, 'Failure', t.p('Validation failure: ' + entities(_.map(validationFailures, f => f.friendlyName).join(''))));
      return;
    }

    options.create(postData).then(function () {
      /*
       * In the future, should specify the id that was added. Then the
       * page can load and display short info on the added thing.
       */
      redirect(context, context.url.query.path, {query: _.extend({}, context.url.query, {added: true})});
    }, function (e) {
      page(context, 'Failure', ['Unable to create entity: ', t.em(String(e)), '.']);
    });
  }).catch(function (e) {
    page(context, 'Submission Error', t.p(['Error: ', e]));
  });
};

var handleAction = function (context, modelInfo) {
  return readFormPost(context).then(function (postData) {
    var action;

    action = modelInfo.actions[postData.action];
    if (!action)
      throw new Error('Action unknown or unspecified.');

    return modelInfo.model.findById(postData.id).then(function (instance) {
      if (!instance)
        throw new Error('Unable to load instance which the action is to be performed upon.');

      return action.execute(context, instance);
    });
  }).catch(function (e) {
    page(context, 'Error', t.p(['Error: ', String((e || {}).message)]));
  });
};

var redirect = function (context, path, options) {
  var pathUrl = url(path, options);
  context.response.writeHead(303, { 'Location': pathUrl });
  body(context, 'Found', t.p(l(path, 'Found', options)));
};
var body = function (context, title, content) {
  content = content || '';
  title = title || '';
  var path = require('path');
  var cwd = process.cwd();
  context.response.end(
    ''
      + '<?xml version="1.0" encoding="utf-8"?>\n'
      + '<!DOCTYPE html>\n'
      + '<html xmlns="http://www.w3.org/1999/xhtml">\n'
      + '  <head>\n'
      + '    <title>' + entities(title) + '</title>\n'
      + '    <meta name="viewport" content="initial-scale=1"/>\n'
      + libs.files.map(function (file) {
        file = path.relative(cwd, file);
        if (/\.js$/.test(file))
          return '<script type="application/javascript" src="' + entities(file) + '"/>';
        if (/\.css$/.test(file))
          return '<link rel="stylesheet" type="text/css" href="' + entities(file) + '"/>';
        return '';
      }).join('\n') + '\n'
      + '    <link rel="shortcut icon" type="image/png" href="favicon.png"></link>\n'
      + '    <script type="application/javascript">\n'
      + '      var turboCaseInfo = ' + JSON.stringify({user: context.user.email || null}) + ';\n'
      + '    </script>'
      + '  </head>\n'
      + '  <body class="' + (context.user.email ? '' : ' anonymous') + '">\n'
      + '    <header>\n'
      + '      <ul><li>' + l('index', 'TurboCase') + '</li></ul>'
      + '      <ul><li id="login-wrapper"><button id="login">Login</button></li>\n'
      + '      <li id="logout-wrapper"><form action="' + entities(url('logout')) + '" method="post"><button id="logout">Logout</button></form></li></ul>\n'
      + '    </header>\n'
      + '    ' + t.h1(title) + '\n'
      + t.renderAsHTML(content)
      + '    <script src="https://login.persona.org/include.js"/>'
      + '    <form id="persona_assertion" method="post" action="' + entities(url('authenticate')) + '"><input type="hidden" name="assertion"/></form>\n'
      + '  </body>\n'
      + '</html>\n');
};
var page = function (context, title, content, options) {
  options = _.extend(
    {
      status: 200
    },
    options);
  context.response.writeHead(
    options.status,
    {
      'Content-Type': 'application/xhtml+xml; charset=utf-8',
      /* https://developer.mozilla.org/en-US/Persona/Quick_setup */
      'X-UA-Compatible': 'IE=Edge'
    });
  body(context, title, content);
};

var l = function(path, content, options) {
  return new t.RenderedHTML('<a href="' + t.renderAsHTML(url(path, options)) + '">' + t.renderAsHTML(content) + '</a>');
};

var readFormPost = function (context) {
  return new Promise(function (resolve, reject) {
    if (context.request.method !== 'POST')
      throw new Error('Not POST');

    var postString = '';
    context.request.on('data', function (chunk) {
      postString += chunk.toString();
    });
    context.request.on('end', function () {
      resolve(postString);
    });
  }).then(postString => Url.parse('?' + postString, true).query);
};

/**
 * \brief
 *   Resolves to a client or null if not found or parameter missing. Shows error if client could not be found.
 */
var clientByParameter = function (context) {
  return new Promise(function (resolve) {
      var clientId = context.url.query.id;
      if (!clientId)
        return page(
          context,
          'Client Error',
          t.p('Missing parameter.'));
    return resolve(
      Client.findById(clientId).then(function (client) {
        if (!client)
          page(
            context,
            'Client Error',
            t.p(['Client ', t.em(clientId), ' not found.']));
        return client;
      }));
  });
};

var serviceSourceByParameter = function (context, parameterName) {
  return Promise.resolve(context.url.query[parameterName]).then(function (serviceSourceId) {
    if (!serviceSourceId)
      return page(
        context,
        'Service Source Error',
        t.p('Missing parameter.'));
    return ServiceSource.findOne(
      {
        where: {id: serviceSourceId},
        include: [Service]
      }).then(function (serviceSource) {
      if (!serviceSource)
        return page(
          context,
          'Service Error',
          t.p(['Service ', t.em(serviceSourceId), ' not found.']));
      return serviceSource;
    });
  });
};

var serviceSourceSelect = function (context, parameterName) {
  if (!context.url.query[parameterName])
    return ServiceSource.findAll({where: {active: true}}).then(function (serviceSources) {
      return page(
        context,
        'Select Service Source',
        t.ul(_.map(serviceSources, function (serviceSource) {
          return t.li(l(context.path, serviceSource.name, {query: _.extend({}, context.url.query, _.zipObject([[parameterName, serviceSource.id]]))}));
        })));
    });

  return serviceSourceByParameter(context, parameterName);
};

var globExplanation = t.p(
  'Simple glob-like wildcard characters (‘?’ matches on char, ‘*’ matches any number of chars (including 0), escape with ‘\\’) supported for string fields.');

var searchBuildWhere = function (context, model) {
  return _.reduce(
    modelInfoByModel(model).columns,
    function (result, column) {
      var value = context.url.query[column.name];
      if (value)
      {
        if (column.type === Sequelize.STRING)
          /* Joe requests wildcard support. */
          value = { $like: SimpleGlob.toSqlLike(value) };
        result[column.name] = value;
      }
      return result;
    },
    {});
};

var findAllBySearch = function (context, model, options) {
  return model.findAll(
    _.assign(
      {},
      options,
      {
        where: searchBuildWhere(context, model)
      }));
};

var modelTable = function (context, model, instances, options) {
  var columnInfos, modelInfo, order;
  options = _.assign({
    columnInfos: {}
    ,orderHook: function (order) {}
    ,renderHooks: {}
  }, options);
  modelInfo = modelInfoByModel(model);

  order = modelInfo.order.slice();
  if (!_.isEmpty(modelInfo.actions))
    order.push('actions');
  options.orderHook(order);
  columnInfos = _.map(order, function (columnName) {
    var column, renderHook;
    column = modelInfo.columns[columnName];
    renderHook = options.renderHooks[columnName];
    return _.defaults({}, options.columnInfos[columnName], column ? {
      header: column.friendlyName
      ,render: function (instance) {
        var renderedValue, value;

        value = instance[columnName];
        renderedValue = column.render(value);
        if (renderHook)
          renderedValue = renderHook(instance, value, renderedValue);
        return renderedValue;
      }
    } : undefined, !_.isEmpty(modelInfo.actions) && columnName === 'actions' ? {
      header: 'Actions'
      ,render: function (instance) {
        return _.map(modelInfo.actions, function (action) {
          if (action.userAllowed(context.user, instance))
            return t.form(
              [
                t.input({attributes: {name: 'id', value: instance.id, type: 'hidden'}})
                ,t.input({attributes: {name: 'action', value: action.id, type: 'hidden'}})
                ,t.button(action.name)
              ],
              {attributes: {action: context.url.href, method: 'post'}});
          return undefined;
        });
      }
    } : undefined);
  });

  return t.table(
    _.map(columnInfos, function (columnInfo) {
      return columnInfo.header;
    }),
    _.map(instances, function (instance) {
      return _.map(columnInfos, function (columnInfo) {
        return t.td(columnInfo.render(instance));
      });
    }));
};

var routes = {
  'index': {
    title: 'Home',
    visibility: 'public',
    action: function (context) {
      page(
        context,
        'Index',
        [
          t.p('Welcome to TurboCase.'),
          t.ul(
          _.filter(
            _.sortBy(_.keys(routes)),
            function (path) {
              return (
                context.user.canAccess(path)
                  && routes[path].title);
            }).map(function (path) {
              return t.li(l(path, routes[path].title));
            })),
        ]);
    }
  },
  'client': {
    title: 'Clients',
    visibility: 'admin',
    action: function (context) {
      var modelInfo = modelInfoByModel(Client);
      findAllBySearch(context, Client).then(function (clients) {
        page(
          context,
          'Clients',
          [
            t.ul([
              t.li(l('client/add', 'Add Client')),
              t.li(l('client/search', 'Search Clients')),
            ]),
            modelTable(context, Client, clients, {
              renderHooks: {
                id: function (instance, value, renderedValue) {
                  return l('client/details', value, {query: {id: value}});
                }
              }
            })
          ]);
      });
    }
  },
  'client/add': {
    visibility: 'admin',
    action: function (context) {
      if (context.request.method === 'POST') {
        /* Handle form submission */
        submission(context, Client);
        return;
      }

      /* Normal request */
      page(
        context,
        'Add Client',
        [
          (context.url.query.added ? t.p('Successfully added.') : ''),
          form(Client, {autofocus: true}),
        ]);
    }
  },
  'client/details': {
    visibility: 'admin',
    action: function (context) {
      var modelInfo = modelInfoByModel(Client);
      clientByParameter(context).then(function (client) {
        if (!client)
          return;
        var formattedName = client.formatName();
        page(
          context,
          'Client ' + formattedName,
          [
            t.div(t.dl(_.map(modelInfo.order, function (key) {
              var columnInfo = modelInfo.columns[key];
              return new t.RenderedHTML(
                t.dt(columnInfo.friendlyName)
                  + t.dd(columnInfo.render(client[key])));
            }))),
            t.div(t.ul([
              t.li(l('client/provide', 'Provide Service', {query:{id: client.id}}))])),
          ]);
      }).catch(function (e) {
        page(
          context,
          'Client Error',
          t.p(['Unable to load client: ', e]));
      });
      return undefined;
    }
  },
  'client/provide': {
    visibility: 'admin',
    action: function (context) {
      clientByParameter(context).then(function (client) {
        if (!client)
          return undefined;

        return serviceSourceSelect(context, 'service_source_id').then(function (serviceSource) {
          if (context.request.method === 'POST')
            return submission(context, ServiceProvision, {
              create: function (data) {
                return ServiceProvision.create(_.extend({}, data, {
                  UserId: context.user.id
                }));
              }
            });

          return page(
            context,
            'Provide to ' + client.formatName(),
            [
              t.p(['Provide a(n) ', t.em(serviceSource.name), ' service to ', t.em(client.formatName()), '.']),
              form(
                ServiceProvision,
                {
                  autofocus: true,
                  pre: [
                    t.input({
                      attributes: {
                        name: 'ClientId',
                        type: 'hidden',
                        value: client.id
                      }
                    }),
                    t.div(_.map(serviceSource.Services, function (service) {
                      if (!service.active)
                        return undefined;

                      return t.div(t.label([
                        service.name,
                        ' ',
                        t.input({
                          attributes: {
                            name: 'ServiceId',
                            type: 'radio',
                            value: service.id
                          }
                        })]));
                    })),]
                }),
            ]);
        });
      }).catch(function (e) {
        page(context, 'Error', t.p(['Error ', e]));
      });
    }
  },
  'provision': {
    visibility: 'admin',
    action: function (context) {
      var modelInfo = modelInfoByModel(ServiceProvision);
      var clientModelInfo = modelInfoByModel(Client);
      var serviceSourceModelInfo = modelInfoByModel(ServiceSource);
      var serviceModelInfo = modelInfoByModel(Service);
      /* Is the user performing an action perchance? */
      if (context.request.method === 'POST')
        return handleAction(context, modelInfo);

      findAllBySearch(context, ServiceProvision, {
        include: [
          Client
          ,{
            model: Service,
            include: [ServiceSource, ]
          }
          ,User
        ],
        order: 'timeProvided'
      }).then(function (serviceProvisions) {
        page(
          context,
          'Client Provisions',
          [
            t.ul(
              t.li(l('provision/search', 'Search Provisions')))
            ,modelTable(
              context,
              ServiceProvision,
              serviceProvisions,
              {
                orderHook: function (order) {
                  /* Insert special columns before the actions column. */
                  order.splice(order.length - 1, 0, 'client', 'source', 'service', 'creator');
                }
                ,columnInfos: {
                  client: {
                    header: 'Client'
                    ,render: function (instance) {
                      return l('client/details', instance.Client.formatName(), { query: { id: instance.Client.id } });
                    }
                  }
                  ,source: {
                    header: 'Source'
                    ,render: function (instance) {
                      return instance.Service.ServiceSource.name;
                    }
                  }
                  ,service: {
                    header: 'Service'
                    ,render: function (instance) {
                      return instance.Service.name;
                    }
                  }
                  ,creator: {
                    header: 'Creator'
                    ,render: function (instance) {
                      return t.code(instance.User.email);
                    }
                  }
                }
              })
          ]);
      });
    }
  },
  'provision/search': {
    visibility: 'admin',
    action: function (context) {
      page(
        context,
        'Search Provisions',
        [
          form(
            ServiceProvision,
            {
              action: url('provision'),
              autofocus: true,
              hideAutoIncrement: false,
              method: 'get'
            }),
          globExplanation]);
    }
  },
  'client/search': {
    visibility: 'admin',
    action: function (context) {
      page(
        context,
        'Search Clients',
        [
          form(
            Client,
            {
              action: url('client'),
              autofocus: true,
              hideAutoIncrement: false,
              method: 'get'
            }),
          globExplanation,
        ]);
    }
  }
  ,'reports': {
    title: 'Reports'
    ,visibility: 'admin'
    ,action: function (context) {
      return page(context, 'Reports', [
        t.ul([
          t.li(l('provision', 'Provisions'))
        ])
      ]);
    }
  }
  ,'service': {
    title: 'Services',
    visibility: 'admin',
    action: function (context) {
      if (context.request.method === 'POST')
        return handleAction(context, modelInfoByModel(Service));

      return ServiceSource.findAll(
        {
          include: [Service]
        }).then(function (serviceSources) {
        page(
          context,
          'Services',
          [
            t.ul([
              t.li(l('service/add', 'Add Service')),
            ])
            ,modelTable(context, Service, _(serviceSources).map(function (serviceSource) {
              return _.map(serviceSource.Services, function (service) {
                var o = {
                  ServiceSource: serviceSource
                };
                _.each(modelInfoByModel(Service).order, function (columnName) {
                  o[columnName] = service[columnName];
                });
                return o;
              });
            }).flatten().map(function (x) {console.log('thing id=' + x.id); return x;}).value(), {
              columnInfos: {
                serviceSource: {
                  header: 'Source'
                  ,render: function (instance) {
                    return instance.ServiceSource.name;
                  }
                }
              }
              ,orderHook: function (order) {
                /* Insert before actions column */
                order.splice(order.length - 1, 0, 'serviceSource');
              }
            })
          ]);
      }).catch(function (e) {
        page(context, 'Error', t.p(['Error: ', e]));
      });
    }
  },
  'service/add': {
    visibility: 'admin',
    action: function (context) {
      serviceSourceSelect(context, 'service_source_id').then(function (serviceSource) {
        if (!serviceSource)
          return undefined;

        if (context.request.method === 'POST')
          return submission(context, Service, {
            create: function (data) {
              return serviceSource.createService(data);
            }
          });

        return page(
          context,
          'Add Service',
          [
            (context.url.query.added ? t.p('Added') : ''),
            t.p(['Add a(n) ', t.em(serviceSource.name), ' (', l('service/add', 'change'), ') service.']),
            form(Service, {autofocus: true}),
          ]);
      });
    }
  },
  'service-source': {
    title: 'Service Sources',
    visibility: 'admin',
    action: function (context) {
      var modelInfo;

      modelInfo = modelInfoByModel(ServiceSource);
      /* Is the user performing an action perchance? */
      if (context.request.method === 'POST')
        return handleAction(context, modelInfo);

      ServiceSource.findAll().then(function (serviceSources) {
        page(
          context,
          'Service Sources',
          [
            t.ul([
              t.li(l('service-source/add', 'Add Source'))
            ])
            ,modelTable(context, ServiceSource, serviceSources)
          ]);
      }).catch(function (e) {
        page(context, 'Error', t.p(['Error: ', e]));
      });
    }
  },
  'service-source/add': {
    visibility: 'admin',
    action: function (context) {
      if (context.request.method === 'POST')
        return submission(context, ServiceSource);

      page(
        context,
        'Add Service Source',
        [
          (context.url.query.added ? t.p('Successfully added.') : ''),
          form(ServiceSource, {autofocus: true}),
        ]);
    }
  },
  'authenticate': {
    visibility: 'public',
    action: function (context) {
      readFormPost(context).then(function (postData) {
        var assertion = postData['assertion'];
        if (!assertion)
          throw new Error('Missing parameter assertion.');
        return new Promise(function (resolve, reject) {
          idpPersona.identity(assertion, function (error, identity) {
            if (error)
              reject(new Error('Unable to validate assertion'));
            else
              resolve(identity.email);
          });
        });
      }).then(function (email) {
        return ensureUser(email);
      }).then(function (user) {
        /* Generate session. */
        return new Promise(function (resolve, reject) {
          Crypto.randomBytes(64, function (ex, buf) {
            if (ex)
              reject(ex);
            else
              resolve(buf.toString('hex'));
          });
        }).then(function (session) {
          return UserSession.create({
            session: session,
            UserId: user.id
          });
        }).then(function (userSession) {
          context.cookies.set('session', userSession.session, {overwrite: true});
          return Promise.resolve(user);
        });
      }).then(function (user) {
        context.user = user;
        page(context, 'Identified', t.p(['You now are identified as ', t.em(entities(user.email)), '. ', l('index', 'Proceed'), ]));
      }).then(null, function (e) {
        redirect(context, 'authenticate/failure', {query: {message: (e || {}).message || 'Unknown error.'}});
      });
    }
  },
  'authenticate/failure': {
    visibility: 'public',
    action: function (context) {
      page(context, 'Authentication Failure', t.p('Unable to verify login.'));
    }
  },
  'logout': {
    visibility: 'public',
    action: function (context) {
      if (context.request.method !== 'POST')
        throw new Error('Not POST');
      var session = context.cookies.get('session');
      (session ? UserSession.findById(context.cookies.get('session')) : Promise.resolve(null)).then(function (userSession) {
        return (userSession ? userSession.destroy() : Promise.resolve(null));
      }).then(function () {
        /* Might as well clean up the cookie. */
        context.cookies.set('session');
        redirect(context, 'index');
      });
    }
  },
  'search': {
    visibility: 'search',
    action: function (context) {
      page(context, 'Search', 'Search!');
    }
  },
  'user': {
    visibility: 'admin',
    action: function (context) {
      /* Display a user and some form stuff. */
      User.find(
        {
          where: {email: context.url.query.email},
          include: [Grant]
        }).then(function (user) {
          if (user == null)
            return page(context, 'User Unknown', t.p(['User ', t.code(context.url.query.email), ' unknown.', ]));

          var preDisplayActions = [];
          if (context.request.method === 'POST') {
            preDisplayActions.push(readFormPost(context).then(function (postData) {
              
            }));
          }

          return page(
            context,
            'User ' + entities(user.email),
            [
              t.form(
                t.fieldset([
                  t.legend('Grants'),
                  t.ul(_(routes).map(r => r.visibility).uniq().toArray().sort().map(g => t.li(t.label([
                    t.input({
                      attributes: {
                        checked: (_.indexBy(user.Grants, ug => ug.grant)[g] ? 'checked' : undefined),
                        name: 'userGrant[]',
                        type: 'checkbox',
                        value: g
                      }
                    }),
                    t.code(g),
                  ])))),
                  t.p(t.button('Submit')),
                ])),
            ]);
      });
    }
  },
  'users': {
    visibility: 'admin',
    action: function (context) {
      /*
       * Load pagination of users, I guess.
       */
      var afterEmail = context.url.query.after;
      User.findAll(
        {
          include: [
            {
              model: Grant
            }
          ],
          limit: 64,
          order: [
            ['email']
          ],
          where: _.extend(
            {},
            afterEmail ? {email: {$gt: afterEmail}} : {})
        }).then(function (users) {
          page(
            context,
            'Users',
            [
              t.ol(
                users.map(user => t.li(l('user', user.email, {query: {id: user.id}})))),
            ]);
        });
    }
  }
};

var userFromSession = function (session) {
  return (
    session
    ? User.findOne(
    {
      include: [
        {
          model: UserSession,
          where: {session: session}
        },
        Grant
      ]
    })
    : Promise.resolve(null)).then(user => Promise.resolve(
      user
        || {
          canAccess: function (path) {
            return userCanAccess(this, path);
          },
          Grants: []
        }));
};

var server = Fcgi.createServer(function(req, res) {
  var context = {
    request: req,
    response: res
  };
  var cookies;
  try
  {
    if (({'GET': true, 'POST': true})[req.method]) {
      cookies = context.cookies = new Cookies(req, res);
      /* Get cookie if supplied. */
      userFromSession(cookies.get('session')).then(function (user) {
        var grant, url;
        context.user = user;
        url = context.url = Url.parse(req.url, true);
        _.defaults(url.query, {path: 'index'});
        if (!routes[url.query.path])
          return page(
            context,
            'Not Found',
            [
              t.p(['Sorry, ', t.code(url.query.path), ' is not known', ]),
            ],
            {status: 400});
        context.path = url.query.path;
        if (user.canAccess(context.path))
          return routes[context.path].action(context);
        return page(
          context,
          'Unauthorized',
          t.p(['Access not permitted. If you are logged out, try logging in or try ', l('index', 'returning home'), '.', ]),
          {status: 403});
      });
    } else {
      page(
        'Unsupported Method',
        t.p(['Unrecognized method: ', String(req.method || '<unspecified>'), ]),
        {status: 501});
    }
  }
  finally
  {
    if (myConfig.dev)
      setTimeout(function () {process.exit(0);}, 8000);
  }
});
Promise.all(startupPromises).then(function () {
  server.listen();
}, function (e) {
  console.error(e);
});
