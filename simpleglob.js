'use strict';

var _ = require('lodash');

var tokenize = function (glob) {
  /*
   * Greedy—try matching first syntax before proceeding.
   */
  var tokenInfo = {
    literal: { appendable: true },
    any: { appendable: true },
    one: { appendable: false }
  };
  var syntax = [
    {
      regexp: /^([^?*\\]+)/,
      type: 'literal'
    },
    {
      regexp: /^\\(.)/,
      type: 'literal'
    },
    {
      /*
       * Condense because we do not distinguish “*” and “***”. And,
       * more important to mention—because some things do treat it
       * differently—we do not treat “**” specially.
       */
      regexp: /^\*+/,
      type: 'any'
    },
    {
      regexp: /^\?/,
      type: 'one'
    },
    /*
     * Fallback case (e.g. trailing backslash): advance at least some.
     */
    {
      regexp: /^(.)/,
      type: 'literal'
    }
  ];
  var tokens = [];
  var lastToken = {};
  var matches;
  var i;
  while (glob.length)
  {
    for (i in syntax)
    {
      matches = syntax[i].regexp.exec(glob);
      if (!matches)
        continue;

      /* Always append to prior token if possible. */
      if (tokenInfo[syntax[i].type].appendable
          && lastToken.type === syntax[i].type)
        lastToken.value += matches[1];
      else
        tokens.push(lastToken = {
          type: syntax[i].type,
          value: matches[1]
        });
      glob = glob.substring(matches[0].length);
      break;
    }
  }
  return tokens;
};

/**
 * Transform a “simpleglob” as I’m going to call it to a LIKE
 * expression.
 *
 * Simple globs are glob expressions which are intended for matching
 * against strings (not for path navigation) and which allow ‘*’ to
 * match zero or any number of characters (includeing ‘/’) and allows
 * ‘?’ to match exactly one character. In simple globs, backslash
 * (‘\’) escapes backslash, star (‘*’), and question (‘?’). Using a
 * backslash followed by any other character produces undefined
 * behavior ^^.
 */
var toSqlLike = exports.toSqlLike = function (glob) {
  return _.map(tokenize(glob), function (token) {
    switch (token.type) {
    case 'literal':
      return token.value.replace(/[_%]/g, '\\$&');
    case 'any':
      return '%';
    case 'one':
      return '_';
    default:
      throw new Error('Unhandled token type: ' + token.type);
    }
  }).join('');
};

/*
 * Any testsuite recommendations?
 */
(function () {
  var assertEqual = function (expected, actual) {
    if (!_.isEqual(expected, actual))
      throw new Error('Assertion failed: expected “' + String(JSON.stringify(expected)) + '”, got “' + String(JSON.stringify(actual)) + '”.');
  };

  assertEqual(
    [
      {
        type: 'literal',
        value: 'hi'
      }
    ],
    tokenize('hi'));

  /*
   * We require our tokenizer to give the simplest representation,
   * which means concatenating adjacent literals even if the tokenizer
   * internally handles it differently.
   */
  assertEqual(
    [
      {
        type: 'literal',
        value: 'literally*'
      }
    ],
    tokenize('literally\\*'));

  /*
   * Easier to test output of another expression type than to keep
   * writing out all these array literals ;-).
   */
  assertEqual('%', toSqlLike('*'));
  assertEqual('_', toSqlLike('?'));
  assertEqual('a%b*', toSqlLike('a*b\\*'));
  assertEqual('100\\%', toSqlLike('100%'));
  assertEqual('1\\_1\\_4', toSqlLike('1_1_4'));
})();
